/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avilages.BouncyCastleCarimboDoTempo;

import com.avilages.DemoiselleAssinatura.Assinatura;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

/**
 *
 * @author Avilages
 */
public class PropriedadesCarimbo {

    private Properties prop;
    private String url;
    private String login;
    private String senha;

    public PropriedadesCarimbo() {

        try {
            // Instanciando propriedade de carimbo do tempo.
            this.prop = getProp();
            // Obtendo dados do servidor de carimbo do tempo.
            this.url = prop.getProperty("URL");
            this.login = prop.getProperty("LOGIN");
            this.senha = prop.getProperty("SENHA");

        } catch (IOException ex) {
            Logger.getLogger("Erro ao abrir arquivo de propriedade de carimbo do tempo.");
        }
    }

    public PropriedadesCarimbo(String url, String login, String senha) {
        this.url = url;
        this.login = login;
        this.senha = senha;
    }

    public String getUrl() {
        return url;
    }

    public String getLogin() {
        return login;
    }

    public String getSenha() {
        return senha;
    }

    /**
     * Metodo responsavel por obter as propriedades da ACT.
     *
     * @return
     * @throws IOException
     */
    public static Properties getProp() throws IOException {
        Properties props = new Properties();
        InputStream file = Assinatura.class.getClass().getResourceAsStream("/carimboDoTempo.properties");
        props.load(file);
        return props;
    }

}
