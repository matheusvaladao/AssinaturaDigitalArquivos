/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avilages.BouncyCastleCarimboDoTempo;

import java.security.Security;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.cms.Attribute;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.tsp.TimeStampToken;
import org.demoiselle.signer.timestamp.Timestamp;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Avilages
 */
public class InformacaoCarimbo {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(InformacaoCarimbo.class);

    /**
     * Metodo que obtem relatorio de carimbos do tempo.
     *
     * @param conteudo
     * @param signature
     * @return
     */
    public String obtemRelatorioCarimboDoTempo(byte[] conteudo, byte[] signature) {

        String retorno = "";

        try {

            Security.addProvider(new BouncyCastleProvider());
            List<Timestamp> listOfTimeStamp = new ArrayList<Timestamp>();

            CMSSignedData cmsSignedData;
            if (conteudo == null) {
                cmsSignedData = new CMSSignedData(signature);
            } else {
                cmsSignedData = new CMSSignedData(new CMSProcessableByteArray(conteudo), signature);
            }

            SignerInformationStore signers = cmsSignedData.getSignerInfos();
            Iterator<?> it = signers.getSigners().iterator();

            while (it.hasNext()) {
                SignerInformation signer = (SignerInformation) it.next();
                AttributeTable unsignedAttributes = signer.getUnsignedAttributes();
                Attribute attributeTimeStamp = unsignedAttributes.get(new ASN1ObjectIdentifier(PKCSObjectIdentifiers.id_aa_signatureTimeStampToken.getId()));

                if (attributeTimeStamp != null) {
                    byte[] varTimeStamp = attributeTimeStamp.getAttrValues().getObjectAt(0).toASN1Primitive().getEncoded();
                    TimeStampToken timeStampToken = new TimeStampToken(new CMSSignedData(varTimeStamp));
                    Timestamp timeStampSigner = new Timestamp(timeStampToken);
                    listOfTimeStamp.add(timeStampSigner);
                }
            }

            if (!listOfTimeStamp.isEmpty()) {
                for (Timestamp ts : listOfTimeStamp) {

                    int pos = ts.getTimeStampAuthorityInfo().indexOf("CN=") + 3;
                    String autoridade = ts.getTimeStampAuthorityInfo().substring(pos, ts.getTimeStampAuthorityInfo().length());
                    retorno = "\nACT: " + autoridade;
                    retorno = retorno + "\nCarimbo do tempo: " + ts.getTimeStamp();
                }
            }

            return retorno;

        } catch (Exception e) {
            logger.error(e.toString());
            return null;
        }

    }

}
