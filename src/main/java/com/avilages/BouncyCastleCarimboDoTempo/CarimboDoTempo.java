/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avilages.BouncyCastleCarimboDoTempo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.cms.Attribute;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.tsp.TimeStampResp;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.CMSTypedData;
import org.bouncycastle.cms.SignerInfoGenerator;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoGeneratorBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampRequest;
import org.bouncycastle.tsp.TimeStampRequestGenerator;
import org.bouncycastle.tsp.TimeStampResponse;
import org.bouncycastle.tsp.TimeStampToken;
import org.bouncycastle.util.Store;
import org.demoiselle.signer.timestamp.Timestamp;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Avilages
 */
public class CarimboDoTempo {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CarimboDoTempo.class);

    public byte[] obterAssinaturaComCarimboDoTempo(byte[] conteudo, byte[] assinatura, Certificate[] certificates, PrivateKey privateKey) {

        byte[] carimboDoTempoRetorno = null;

        try {

            logger.info("Montando objeto CMS");
            Security.addProvider(new BouncyCastleProvider());
            CMSSignedData cmsSignedData;
            if (conteudo == null) {
                cmsSignedData = new CMSSignedData(assinatura);
            } else {
                cmsSignedData = new CMSSignedData(new CMSProcessableByteArray(conteudo), assinatura);
            }

            // Obtem os atributos não assinados.
            logger.info("Obtendo os atributos não assinados.");
            ASN1EncodableVector unsignedAttributes = new ASN1EncodableVector();
            Collection<SignerInformation> vNewSigners = cmsSignedData.getSignerInfos().getSigners();
            Iterator<SignerInformation> it = vNewSigners.iterator();
            SignerInformation oSi = it.next();

            // Obtem carimbo do tempo para adicionar nos atributos da assinatura.
            logger.info("Obtendo carimbo do tempo para adicionar nos atributos da assinatura.");
            unsignedAttributes.add(obterTimesTamp(privateKey, certificates, oSi.getSignature()));
            logger.info("Carimbo do tempo adicionado nos atributos nao assinados.");
            AttributeTable at = new AttributeTable(unsignedAttributes);
            vNewSigners.remove(oSi);
            oSi = SignerInformation.replaceUnsignedAttributes(oSi, at);
            vNewSigners.add(oSi);

            SignerInformationStore oNewSignerInformationStore = new SignerInformationStore(vNewSigners);
            CMSSignedData oSignedData = cmsSignedData;
            cmsSignedData = CMSSignedData.replaceSigners(oSignedData, oNewSignerInformationStore);

            return cmsSignedData.getEncoded();

        } catch (Exception e) {
            logger.error("Erro ao obter carimbo do tempo: \n" + e);
            return null;
        }

    }

    /**
     * Metodo que ira obter o carimbo do tempo de uma carimbadora de tempo.
     *
     * @param privateKey
     * @param certificates
     * @param escTimeStampContent
     * @return
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    private Attribute obterTimesTamp(PrivateKey privateKey, Certificate[] certificates, byte[] escTimeStampContent) throws NoSuchAlgorithmException, IOException, TSPException, NoSuchProviderException {

        // Criando requisicao:
        logger.info("Criando requisicao.");
        MessageDigest digest = MessageDigest.getInstance("SHA-256", "BC");
        byte[] hashedMessage = digest.digest(escTimeStampContent);
        TimeStampRequestGenerator timeStampRequestGenerator = new TimeStampRequestGenerator();
        timeStampRequestGenerator.setReqPolicy(new ASN1ObjectIdentifier("2.16.76.1.6.6"));
        timeStampRequestGenerator.setCertReq(true);
        BigInteger nonce = BigInteger.valueOf(System.currentTimeMillis());
        TimeStampRequest timeStampRequest = timeStampRequestGenerator.generate(new ASN1ObjectIdentifier("2.16.840.1.101.3.4.2.1"), hashedMessage, nonce);
        byte request[] = timeStampRequest.getEncoded();

        logger.info("Assinando requisicao para enviar a autoridade carimbadora.");
        //byte[] signedRequest = assinarRequisicao(privateKey, certificates, request, "SHA256withRSA");

        logger.info("Obtendo TimeStampResponse.");
        TimeStampResponse response = obterTSResponse(request);

        logger.info("Validando resposta.");
        response.validate(timeStampRequest);

        logger.info("Obtendo TimeStampToken.");
        TimeStampToken timeStampToken = response.getTimeStampToken();

        // Retorna o atributo com o carimbo do tempo incluso.
        Timestamp ts = new Timestamp(timeStampToken);
        // Retorna carimbo do tempo.
        logger.info("Retornando carimbo do tempo.");
        return new Attribute(new ASN1ObjectIdentifier("1.2.840.113549.1.9.16.2.14"), new DERSet(ASN1Primitive.fromByteArray(ts.getEncoded())));
    }

    /**
     * Metodo que ira assinar a requisicao que será enviada a carimbadora.
     *
     * @param privateKey
     * @param certificates
     * @param request
     * @param algorithm
     * @return
     */
    private byte[] assinarRequisicao(PrivateKey privateKey, Certificate[] certificates, byte[] request, String algorithm) {
        try {

            Security.addProvider(new BouncyCastleProvider());

            X509Certificate signCert = (X509Certificate) certificates[0];
            List<X509Certificate> certList = new ArrayList<>();
            certList.add(signCert);

            CMSSignedDataGenerator generator = new CMSSignedDataGenerator();
            String varAlgorithm = null;
            if (algorithm != null && !algorithm.isEmpty()) {
                varAlgorithm = algorithm;
            } else {
                varAlgorithm = "SHA256withRSA";
            }

            SignerInfoGenerator signerInfoGenerator = new JcaSimpleSignerInfoGeneratorBuilder().build(varAlgorithm, privateKey, signCert);
            generator.addSignerInfoGenerator(signerInfoGenerator);

            Store<?> certStore = new JcaCertStore(certList);
            generator.addCertificates(certStore);

            CMSTypedData data = new CMSProcessableByteArray(request);
            CMSSignedData signed = generator.generate(data, true);
            return signed.getEncoded();

        } catch (Exception ex) {
            logger.info(ex.getMessage());
        }
        return null;
    }

    /**
     * Metodo que ira enviar um requisicao ao servidor de carimbo do tempo e ira
     * obter a resposta em formato tsReponse.
     *
     * @param requestBytes
     * @return
     */
    private TimeStampResponse obterTSResponse(byte[] requestBytes) throws MalformedURLException, IOException, TSPException {

        logger.info("Criando conexao com ACT");
        PropriedadesCarimbo prop = new PropriedadesCarimbo("http://act.bry.com.br", "04778207190", "avilagesbry2018");
        String autenticacao = prop.getLogin() + ":" + prop.getSenha();

        URL url = new URL(prop.getUrl());
        URLConnection con = url.openConnection();
        con.setDoOutput(true);
        con.setDoInput(true);
        con.setUseCaches(false);
        con.setRequestProperty("Content-Type", "application/timestamp-query");
        con.setRequestProperty("Content-Transfer-Encoding", "binary");
        con.setRequestProperty("Authorization", "Basic " + Base64.getEncoder().encodeToString(autenticacao.getBytes()));
        con.connect();

        logger.info("Enviando dados a ACT.");
        OutputStream output = null;
        try {
            output = con.getOutputStream();
            output.write(requestBytes, 0, requestBytes.length);
        } finally {
            output.close();
        }

        logger.info("Recebendo dados da ACT.");
        InputStream input = con.getInputStream();

        TimeStampResp resp = TimeStampResp.getInstance(new ASN1InputStream(input).readObject());
        TimeStampResponse response = new TimeStampResponse(resp);
        return response;

    }

    /**
     * Metodo que verifica os carimbos do tempo e retorna a lista com os
     * carimbos.
     *
     * @param conteudo
     * @param signature
     * @return
     */
    public boolean checarCarimboDoTempo(byte[] conteudo, byte[] signature) {

        boolean retorno = false;
        try {

            Security.addProvider(new BouncyCastleProvider());

            CMSSignedData cmsSignedData;
            if (conteudo == null) {
                cmsSignedData = new CMSSignedData(signature);
            } else {
                cmsSignedData = new CMSSignedData(new CMSProcessableByteArray(conteudo), signature);
            }

            SignerInformationStore signers = cmsSignedData.getSignerInfos();
            Iterator<?> it = signers.getSigners().iterator();

            while (it.hasNext()) {

                SignerInformation signer = (SignerInformation) it.next();
                AttributeTable unsignedAttributes = signer.getUnsignedAttributes();
                Attribute attributeTimeStamp = unsignedAttributes.get(new ASN1ObjectIdentifier(PKCSObjectIdentifiers.id_aa_signatureTimeStampToken.getId()));

                if (attributeTimeStamp != null) {

                    byte[] varTimeStamp = attributeTimeStamp.getAttrValues().getObjectAt(0).toASN1Primitive().getEncoded();
                    TimeStampToken timeStampToken = new TimeStampToken(new CMSSignedData(varTimeStamp));
                    Timestamp timeStampSigner = new Timestamp(timeStampToken);

                    //Onde se verifica o carimbo do tempo.
                    MessageDigest digest = MessageDigest.getInstance("SHA-256");
                    byte[] calculatedHash = digest.digest(signer.getSignature());
                    if (Arrays.equals(calculatedHash, timeStampToken.getTimeStampInfo().getMessageImprintDigest())) {
                        retorno = true;
                        logger.info("Hash do carimbo do tempo está correto.");
                    } else {
                        retorno = false;
                        throw new Exception("Erro ao verificar hash incluso no carimbo do tempo.");
                    }
                }
            }

            return retorno;

        } catch (Exception e) {
            logger.error(e.toString());
            return false;
        }

    }
}
