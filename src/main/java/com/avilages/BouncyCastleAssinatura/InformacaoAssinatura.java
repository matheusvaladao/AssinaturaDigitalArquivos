/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avilages.BouncyCastleAssinatura;

import com.avilages.BouncyCastleCarimboDoTempo.InformacaoCarimbo;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.asn1.ASN1UTCTime;
import org.bouncycastle.asn1.DERUTCTime;
import org.bouncycastle.asn1.cms.Attribute;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.cms.CMSAttributes;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.util.Store;

/**
 *
 * @author Avilages
 */
public class InformacaoAssinatura {

    /**
     * Método que retorna a informação do pacote pkcs7.
     *
     * @param conteudo
     * @param assinatura
     * @return
     */
    public String retornaInformacao(byte[] conteudo, byte[] assinatura) throws CMSException, CertificateParsingException, InvalidNameException, IOException, Exception {

        // String que retornara o relatorio.
        String retorno = "";
        // Instanciando CMSSignerData.
        CMSSignedData cmsSignedData = cmsSignedData = new CMSSignedData(new CMSProcessableByteArray(conteudo), assinatura);;
        // Carregando loja de certificados.
        Store<?> certStore = cmsSignedData.getCertificates();
        // Carregando store de informacoes.
        SignerInformationStore signers = cmsSignedData.getSignerInfos();
        // Carregando as informacoes da assinatura.
        Collection<SignerInformation> informacaoAssinatura = signers.getSigners();
        // Instanciando iterator.
        Iterator<?> it = signers.getSigners().iterator();
        // Instanciando list de certificados.
        List<X509Certificate> certificados = new LinkedList<X509Certificate>();
        //Pergcorrendo dados e montando cadeia de certificados.
        while (it.hasNext()) {
            SignerInformation signer = (SignerInformation) it.next();
            Collection<?> certCollection = certStore.getMatches(signer.getSID());
            Iterator<?> certIt = certCollection.iterator();
            X509CertificateHolder certificateHolder = (X509CertificateHolder) certIt.next();
            try {
                certificados.add(new JcaX509CertificateConverter().getCertificate(certificateHolder));
            } catch (CertificateException error) {
            }
        }
        // Obtendo o certificado relativo ao titular.
        X509Certificate certificate = certificados.get(0);
        X500Principal principal = certificate.getIssuerX500Principal();
        Collection<List<?>> informacoes = certificate.getSubjectAlternativeNames();

        String nomeCertificado = "";
        String dn = certificate.getSubjectX500Principal().getName();
        LdapName ldapDN = new LdapName(dn);
        for (Rdn rdn : ldapDN.getRdns()) {
            if (rdn.getType().equalsIgnoreCase("CN")) {
                nomeCertificado = rdn.getValue().toString();
                break;
            }
        }

        String autoridadeCertidicadora = "";
        dn = certificate.getIssuerDN().getName();
        ldapDN = new LdapName(dn);
        for (Rdn rdn : ldapDN.getRdns()) {
            if (rdn.getType().equalsIgnoreCase("CN")) {
                autoridadeCertidicadora = rdn.getValue().toString();
                break;
            }
        }

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String dataValidadeInicio = df.format(certificate.getNotBefore());
        String dataValidadeFim = df.format(certificate.getNotAfter());

        Date dataAssinatura = new Date();
        for (SignerInformation signerInformation : informacaoAssinatura) {
            AttributeTable signedAttr = signerInformation.getSignedAttributes();
            Attribute signingTime = signedAttr.get(CMSAttributes.signingTime);
            if (signingTime != null) {
                Enumeration en = signingTime.getAttrValues().getObjects();
                while (en.hasMoreElements()) {
                    Object obj = en.nextElement();
                    ASN1UTCTime time = (ASN1UTCTime) obj;
                    dataAssinatura = time.getDate();
                    if (obj instanceof ASN1UTCTime) {
                        ASN1UTCTime asn1Time = (ASN1UTCTime) obj;
                    } else if (obj instanceof DERUTCTime) {
                        DERUTCTime derTime = (DERUTCTime) obj;
                    }
                }
            } else {
                System.out.println("Data de assinatura nao encontrada.");
            }
        }
        String dataAssinaturaS = df.format(dataAssinatura);

        String algoritmoUtilizado = certificate.getSigAlgName();

        InformacaoCarimbo infoCarimbo = new InformacaoCarimbo();
        String carimboDoTempo = infoCarimbo.obtemRelatorioCarimboDoTempo(conteudo, assinatura);

        // Obtendo dados relativos ao certificado:
        OIDExtensionUtil dados = new OIDExtensionUtil(certificate);

        // Pessoa juridica:
        String cnpj = dados.getValue("2.16.76.1.3.3");
        String email = "";
        for (final List<?> item : informacoes) {
            Object value = item.get(1);
            if (value instanceof String) {
                email = (String) value;
            }
        }

        // Pessoa fisica:
        String responsavelPessoaFisica = dados.getValue("2.16.76.1.3.2");
        String cpf = dados.getValue("2.16.76.1.3.4", 8, 19);
        String dataNascimento = (dados.getValue("2.16.76.1.3.4", 0, 2)) + "/" + (dados.getValue("2.16.76.1.3.4", 2, 4)) + "/" + (dados.getValue("2.16.76.1.3.4", 4, 8));

        // Gerando Relatório
        retorno = retorno + "\nNome do certificado: " + nomeCertificado;
        retorno = retorno + "\nAutoridade Certificadora: " + autoridadeCertidicadora;
        retorno = retorno + "\nValidade do certificado: " + dataValidadeInicio + " - " + dataValidadeFim;
        retorno = retorno + "\nData da assinatura: " + dataAssinaturaS;
        retorno = retorno + "\nAlgorítmo utilizado: " + algoritmoUtilizado;
        retorno = retorno + carimboDoTempo;
        retorno = retorno + "\n\nDados de pessoa jurídica:\n";
        retorno = retorno + "\nTitular: " + nomeCertificado;
        retorno = retorno + "\nCNPJ: " + cnpj;
        retorno = retorno + "\nE-mail: " + email;
        retorno = retorno + "\n\nDados de pessoa física:\n";
        retorno = retorno + "\nNome do responsável: " + responsavelPessoaFisica;
        retorno = retorno + "\nCPF: " + cpf;
        retorno = retorno + "\nData de nascimento do responsável: " + dataNascimento;

        return retorno;
    }

}
