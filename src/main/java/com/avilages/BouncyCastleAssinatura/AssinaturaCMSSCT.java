/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avilages.BouncyCastleAssinatura;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.UnrecoverableEntryException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.DERUTCTime;
import org.bouncycastle.asn1.cms.Attribute;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.cms.CMSAttributes;
import org.bouncycastle.asn1.esf.OtherHashAlgAndValue;
import org.bouncycastle.asn1.esf.SigPolicyQualifierInfo;
import org.bouncycastle.asn1.esf.SigPolicyQualifiers;
import org.bouncycastle.asn1.esf.SignaturePolicyId;
import org.bouncycastle.asn1.ess.ESSCertIDv2;
import org.bouncycastle.asn1.ess.SigningCertificateV2;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.CMSTypedData;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.util.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Avilages
 */
public class AssinaturaCMSSCT {

    /**
     * As informações que devem ser tratadas como parâmetros de configuração
     * foram extraídas como constantes.
     */
    private static final Logger logger = LoggerFactory.getLogger(AssinaturaCMSSCT.class);
    private static final String SIGNATUREALGO = "SHA256withRSA";
    private static final String SENHA_KEYSTORE = "123456";

    /**
     * Metodo de assinatura digital com certificado A1.
     *
     * @param conteudo
     * @param certificadoA1
     * @param senha
     * @return
     * @throws KeyStoreException
     * @throws NoSuchProviderException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws CertificateException
     * @throws UnrecoverableEntryException
     * @throws CMSException
     * @throws org.bouncycastle.operator.OperatorCreationException
     */
    public byte[] assinarA1(byte[] conteudo, FileInputStream certificadoA1, String senha) throws KeyStoreException, NoSuchProviderException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableEntryException, CMSException, OperatorCreationException, Exception {
        // Assinatura a ser devolvida.
        byte[] assinatura = null;
        // Instanciando a KeyStore.
        KeyStore ks = KeyStore.getInstance("pkcs12");
        // Carregando Certificados da KeyStore.
        ks.load(certificadoA1, senha.toCharArray());
        //Instanciando entrada de chaves privadas
        KeyStore.PrivateKeyEntry pkEntry = null;
        //Instanciando privateKey
        PrivateKey privateKey = null;
        //Instanciando certificados.
        Certificate[] certificates = null;
        //Instanciando enumeracao de nomes amigaveis. ALIAS.
        Enumeration<String> aliasesEnum = ks.aliases();
        //Percorrendo alias disponiveis na key store.
        while (aliasesEnum.hasMoreElements()) {
            String alias = (String) aliasesEnum.nextElement();
            if (ks.isKeyEntry(alias)) {
                pkEntry = (KeyStore.PrivateKeyEntry) ks.getEntry(alias, new KeyStore.PasswordProtection(senha.toCharArray()));
                privateKey = pkEntry.getPrivateKey();
                certificates = ks.getCertificateChain(alias);
                break;
            }
        }

        Certificate certificate = certificates[0];
        X509Certificate c509 = (X509Certificate) certificate;
        Date iniDate = c509.getNotBefore();
        Date fimDate = c509.getNotAfter();
        Date dataAtual = new Date();
        if (!(dataAtual.after(iniDate) && dataAtual.before(fimDate))) {
            //Certificado fora do período de validade!
            return null;
        }

        // Obtendo generador de assinatura com os atributos da mesma.
        CMSSignedDataGenerator cms = setUpProvider(certificate, privateKey);
        // Assinando conteudo. -> True = Attached; False = Dettached. 
        CMSSignedData signedData = signPkcs7(conteudo, cms, false);
        // Adicionando atributos a assinatura.
        signedData = carregaAtributosCMSSCT(certificate, signedData);
        // Obtendo bytes da assinatura.
        assinatura = signedData.getEncoded();

        logger.info("Padrão CMS_SCT criado e assinado.");

        return assinatura;
    }

    /**
     * Metodo de assinatura digital.
     *
     * @param conteudo
     * @param certificadoAlias
     * @return
     * @throws KeyStoreException
     * @throws NoSuchProviderException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws CertificateException
     * @throws UnrecoverableEntryException
     * @throws CMSException
     */
    public byte[] assinarA3(byte[] conteudo, String certificadoAlias) throws KeyStoreException, NoSuchProviderException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableEntryException, CMSException, OperatorCreationException, Exception {
        // Assinatura a ser devolvida.
        byte[] assinatura = null;
        // Instanciando a KeyStore.
        KeyStore ks = KeyStore.getInstance("Windows-MY", "SunMSCAPI");
        // Carregando Certificados da KeyStore.
        ks.load(null, null);
        // Obtendo todas as alias de certificados instalados no computador.
        Enumeration<String> aliasesEnum = ks.aliases();

        // Percorrendo todos os certificados da keyStore.
        while (aliasesEnum.hasMoreElements()) {

            // Verificando se o certificado for o certificado de nome repassado.
            if (ks.isKeyEntry(certificadoAlias)) {
                // Obtendo a cadeia de certificados.
                Certificate[] certificates = ks.getCertificateChain(certificadoAlias);

                //Verificando validade do certificado.
                Certificate certificate = certificates[0];
                X509Certificate c509 = (X509Certificate) certificate;
                Date iniDate = c509.getNotBefore();
                Date fimDate = c509.getNotAfter();
                Date dataAtual = new Date();
                if (!(dataAtual.after(iniDate) && dataAtual.before(fimDate))) {
                    //Certificado fora do período de validade!
                    return null;
                }
                // Carregando a entrada de chaves privadas.
                KeyStore.PrivateKeyEntry pkEntry = (KeyStore.PrivateKeyEntry) ks.getEntry(certificadoAlias, new KeyStore.PasswordProtection(SENHA_KEYSTORE.toCharArray()));
                // Obtendo a chave privada.
                PrivateKey privateKey = pkEntry.getPrivateKey();
                // Obtendo generator.
                CMSSignedDataGenerator cms = setUpProvider(certificate, privateKey);
                // Assinando conteudo. -> True = Attached; False = Dettached. 
                CMSSignedData signedData = signPkcs7(conteudo, cms, false);
                // Adicionando atributos a assinatura.
                signedData = carregaAtributosCMSSCT(certificate, signedData);
                // Obtendo bytes da assinatura.
                assinatura = signedData.getEncoded();

                logger.info("Padrão CMS_SCT criado e assinado.");

                return assinatura;

            }
        }
        // Retorna null caso não seja assinado.
        return assinatura;
    }

    /**
     * Metodo que obtem cria o objeto CMSSignedDataGenerator ou Generator.
     *
     * @param certificate
     * @param privatekey
     * @return
     * @throws Exception
     */
    private CMSSignedDataGenerator setUpProvider(Certificate certificate, PrivateKey privatekey) throws Exception {

        Security.addProvider(new BouncyCastleProvider());

        List<Certificate> certlist = Collections.singletonList(certificate);

        Store certstore = new JcaCertStore(certlist);

        ContentSigner signer = new JcaContentSignerBuilder(SIGNATUREALGO).build(privatekey);

        CMSSignedDataGenerator generator = new CMSSignedDataGenerator();

        generator.addSignerInfoGenerator(new JcaSignerInfoGeneratorBuilder(new JcaDigestCalculatorProviderBuilder().build()).build(signer, (X509Certificate) certificate));

        generator.addCertificates(certstore);

        return generator;
    }

    /**
     * Metodo que realiza a assinatura digital.
     *
     * @param content
     * @param generator
     * @param attached
     * @return
     * @throws Exception
     */
    private CMSSignedData signPkcs7(final byte[] content, final CMSSignedDataGenerator generator, boolean attached) throws Exception {

        CMSTypedData cmsdata = new CMSProcessableByteArray(content);
        CMSSignedData signeddata = generator.generate(cmsdata, attached);

        return signeddata;
    }

    /**
     * Metodo que carrega e adiciona os atributos da assinatura digital.
     *
     * @param certificate
     * @param signedData
     * @return
     * @throws CertificateEncodingException
     */
    private CMSSignedData carregaAtributosCMSSCT(Certificate certificate, CMSSignedData signedData) throws CertificateEncodingException, NoSuchAlgorithmException, NoSuchProviderException {

        // Parametro de algoritmo SHA_256
        OtherHashAlgAndValue otherHashAlgAndValue = new OtherHashAlgAndValue(new AlgorithmIdentifier(NISTObjectIdentifiers.id_sha256), new DEROctetString(new BigInteger("0F6FA2C6281981716C95C79899039844523B1C61C2C962289CDAC7811FEEE29E", 16).toByteArray()));
        // Parametro de Qualifiers
        SigPolicyQualifierInfo policyQualifierInfo = new SigPolicyQualifierInfo(PKCSObjectIdentifiers.id_spq_ets_uri, new DERIA5String("http://politicas.icpbrasil.gov.br/PA_AD_RB_v2_2.der"));
        SigPolicyQualifierInfo[] qualifierInfos = new SigPolicyQualifierInfo[]{policyQualifierInfo};
        SigPolicyQualifiers qualifiers = new SigPolicyQualifiers(qualifierInfos);
        SignaturePolicyId signaturePolicyId = new SignaturePolicyId(new ASN1ObjectIdentifier("2.16.76.1.7.1.1.2.2"), otherHashAlgAndValue, qualifiers);

        Date date = new Date();

        java.security.MessageDigest mDigest = java.security.MessageDigest.getInstance("SHA-256", "BC");
        byte[] digest = mDigest.digest(certificate.getEncoded());

        ESSCertIDv2 essCert = new ESSCertIDv2(new AlgorithmIdentifier(NISTObjectIdentifiers.id_sha256), digest);
        SigningCertificateV2 scv2 = new SigningCertificateV2(new ESSCertIDv2[]{essCert});

        ASN1EncodableVector signedAttributes = new ASN1EncodableVector();
        signedAttributes.add(new Attribute(CMSAttributes.contentType, new DERSet(new ASN1ObjectIdentifier("1.2.840.113549.1.7.1"))));
        signedAttributes.add(new Attribute(CMSAttributes.signingTime, new DERSet(new DERUTCTime(date))));
        signedAttributes.add(new Attribute(PKCSObjectIdentifiers.id_aa_ets_sigPolicyId, new DERSet(signaturePolicyId)));
        signedAttributes.add(new Attribute(PKCSObjectIdentifiers.id_aa_signingCertificateV2, new DERSet(scv2)));

        AttributeTable at = new AttributeTable(signedAttributes);
        Collection<SignerInformation> vNewSigners = signedData.getSignerInfos().getSigners();
        Iterator<SignerInformation> it = vNewSigners.iterator();
        SignerInformation oSi = it.next();
        SignerInformationStore oNewSignerInformationStore = new SignerInformationStore(vNewSigners);
        vNewSigners.remove(oSi);
        oSi = SignerInformation.replaceUnsignedAttributes(oSi, at);
        vNewSigners.add(oSi);
        CMSSignedData oSignedData = signedData;
        signedData = CMSSignedData.replaceSigners(oSignedData, oNewSignerInformationStore);

        return signedData;
    }

    /**
     * Metodo que obtem a data de expiração do certificado.
     *
     * @param alias
     * @param keystore
     * @return
     * @throws KeyStoreException
     */
    private Date getExpiration(String alias, KeyStore keystore) throws KeyStoreException {
        Date date = ((X509Certificate) keystore.getCertificate(alias)).getNotAfter();
        return date;
    }

    /**
     * Metodo que valida ou não a assinatura digital Se for attached, passar
     * conteudo nulo. Se for dettached, passar conteudo.
     *
     * @param conteudo
     * @param assinatura
     * @return
     */
    public boolean validar(byte[] conteudo, byte[] assinatura) throws CMSException {

        logger.info("Validando assinatura.");
        boolean retorno = true;

        Security.addProvider(new BouncyCastleProvider());
        CMSSignedData cmsSignedData;
        if (conteudo == null) {
            cmsSignedData = new CMSSignedData(assinatura);
        } else {
            cmsSignedData = new CMSSignedData(new CMSProcessableByteArray(conteudo), assinatura);
        }

        // Carregando loja de certificados.
        Store<?> certStore = cmsSignedData.getCertificates();
        // Carregando store de informacoes.
        SignerInformationStore signers = cmsSignedData.getSignerInfos();
        // Instanciando iterator.
        Iterator<?> it = signers.getSigners().iterator();

        logger.info("Comecando a validar assinaturas.");
        // Realização da verificação básica de todas as assinaturas
        while (it.hasNext()) {
            try {
                SignerInformation signerInfo = (SignerInformation) it.next();
                SignerInformationStore signerInfoStore = signerInfo.getCounterSignatures();

                Collection<?> certCollection = certStore.getMatches(signerInfo.getSID());

                Iterator<?> certIt = certCollection.iterator();
                X509CertificateHolder certificateHolder = (X509CertificateHolder) certIt.next();

                if (!signerInfo.verify(new JcaSimpleSignerInfoVerifierBuilder().setProvider("BC").build(certificateHolder))) {
                    logger.info("Assinatura nao correta.");
                    retorno = false;
                }

            } catch (Exception e) {
                retorno = false;
            }
        }
        logger.info("Retornando valor da verificacao.");
        return retorno;
    }

}
