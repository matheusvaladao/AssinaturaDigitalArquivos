/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avilages.DemoiselleAssinatura;

import org.demoiselle.signer.core.extension.ICPBrasilExtension;
import org.demoiselle.signer.core.extension.ICPBrasilExtensionType;

public class CertICPBrasil {

    @ICPBrasilExtension(type = ICPBrasilExtensionType.NAME)
    private String titular;

    @ICPBrasilExtension(type = ICPBrasilExtensionType.EMAIL)
    private String email;

    @ICPBrasilExtension(type = ICPBrasilExtensionType.CNPJ)
    private String cnpj;

    @ICPBrasilExtension(type = ICPBrasilExtensionType.CPF_RESPONSIBLE_PESSOA_JURIDICA)
    private String cpfResponsavel;

    @ICPBrasilExtension(type = ICPBrasilExtensionType.NAME_RESPONSIBLE_PESSOA_JURIDICA)
    private String nomeResponsavel;

    @ICPBrasilExtension(type = ICPBrasilExtensionType.BIRTH_DATE)
    private String dataNasimcentoResponsavel;

    @ICPBrasilExtension(type = ICPBrasilExtensionType.ID_NUMBER)
    private String rgPessoaFisica;

    @ICPBrasilExtension(type = ICPBrasilExtensionType.IDENTITY_DISPATCHER)
    private String orgaoEmissorRgPessoaFisica;

    public String getTitular() {
        return titular;
    }

    public String getEmail() {
        return email;
    }

    public String getCnpj() {
        return cnpj;
    }

    public String getCpfResponsavel() {
        return cpfResponsavel;
    }

    public String getNomeResponsavel() {
        return nomeResponsavel;
    }

    public String getDataNasimcentoResponsavel() {
        return dataNasimcentoResponsavel;
    }

    public String getRgPessoaFisica() {
        return rgPessoaFisica;
    }

    public String getOrgaoEmissorRgPessoaFisica() {
        return orgaoEmissorRgPessoaFisica;
    }

}
