/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avilages.DemoiselleAssinatura;

import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.demoiselle.signer.core.CertificateManager;
import org.demoiselle.signer.policy.impl.cades.SignatureInformations;
import org.demoiselle.signer.policy.impl.cades.pkcs7.impl.CAdESChecker;

/**
 *
 * @author Avilages
 */
public class InformacaoAssinatura {

    /**
     * Método que retorna a informação do pacote pkcs7.
     *
     * @param conteudo
     * @param assinatura
     * @return
     */
    public String retornaInformacao(byte[] conteudo, byte[] assinatura) {
        // String que retornara o relatorio.
        String retorno = "";
        // Instanciando checador de assinatura. 
        CAdESChecker checker = new CAdESChecker();
        // Carregando as informacoes da assinatura.
        List<SignatureInformations> informacaoAssinatura = checker.checkDetachedSignature(conteudo, assinatura);
        // Obtendo a cadeia de certificados da assinatura.
        List<X509Certificate> certificados = informacaoAssinatura.get(0).getChain();
        // Obtendo o certificado relativo ao titular.
        X509Certificate certificate = certificados.get(0);
        CertificateManager cm = new CertificateManager(certificate);
        CertICPBrasil cert = cm.load(CertICPBrasil.class);
        // Obtendo o certificado relativo a autoridade certificadora.
        X509Certificate certificateFinal = certificados.get(certificados.size() - 3);
        CertificateManager cmFinal = new CertificateManager(certificateFinal);
        CertICPBrasil certFinal = cmFinal.load(CertICPBrasil.class);
        // Instanciando um formato para a data da assinatura.
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        // Obtendo a data de inicio de validade do certificado.
        Date dataEntrada = (certificate.getNotBefore());
        String dataE = df.format(dataEntrada);
        // Obtendo a data de fim de validade do certificado.
        Date dataVencimento = (certificate.getNotAfter());
        String dataV = df.format(dataVencimento);
        // Obtendo politica da assinatura.
        String politicaAssinatura = informacaoAssinatura.get(0).getSignaturePolicy().getSignPolicyURI();
        // Obtendo data da assinatura:
        Date dataAssinatura = informacaoAssinatura.get(0).getSignDate();

        //Gerando Relatório
        retorno = retorno + "\nAutoridade Certificadora: " + certFinal.getTitular();
        System.out.println(retorno);
        retorno = retorno + "\nValidade do certificado: " + dataE + " - " + dataV;
        retorno = retorno + "\nData da assinatura: " + df.format(dataAssinatura);
        retorno = retorno + "\nPolítica utilizada: " + politicaAssinatura;
        retorno = retorno + "\nAlgorítmo utilizado: " + certificate.getSigAlgName();
        retorno = retorno + "\n\nDados de pessoa jurídica:\n";
        retorno = retorno + "\nTitular: " + cert.getTitular();
        retorno = retorno + "\nE-mail: " + cert.getEmail();
        retorno = retorno + "\nCNPJ: " + cert.getCnpj();
        retorno = retorno + "\nNome do responsável: " + cert.getNomeResponsavel();
        retorno = retorno + "\nCPF: " + cert.getCpfResponsavel();
        retorno = retorno + "\nData de nascimento do responsável: " + cert.getDataNasimcentoResponsavel();
        retorno = retorno + "\n\nDados de pessoa física:\n";
        retorno = retorno + "\nTitular: " + cert.getTitular();
        retorno = retorno + "\nRG: " + cert.getRgPessoaFisica();
        retorno = retorno + "\nOrgão emissor do RG: " + cert.getOrgaoEmissorRgPessoaFisica();

        return retorno;
    }

}
