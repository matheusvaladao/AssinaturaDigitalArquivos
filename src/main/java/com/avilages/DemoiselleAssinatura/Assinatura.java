/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avilages.DemoiselleAssinatura;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStore.PasswordProtection;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.UnrecoverableEntryException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.util.Enumeration;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.tsp.TSPException;
import org.demoiselle.signer.policy.engine.factory.PolicyFactory;
import org.demoiselle.signer.policy.impl.cades.SignerAlgorithmEnum;
import org.demoiselle.signer.policy.impl.cades.factory.PKCS7Factory;
import org.demoiselle.signer.policy.impl.cades.pkcs7.PKCS7Signer;
import org.demoiselle.signer.policy.impl.cades.pkcs7.impl.CAdESChecker;

/**
 *
 * @author Matheus
 */
public class Assinatura {

    /**
     * As informações que devem ser tratadas como parâmetros de configuração
     * foram extraídas como constantes.
     */
    private static final String SENHA_KEYSTORE = "123456";

    public byte[] assinarA3(byte[] conteudo, String certificadoAlias) throws KeyStoreException, NoSuchProviderException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableEntryException, CMSException, OperatorCreationException, CertificateEncodingException, TSPException {

        //Instanciando a KeyStore.
        KeyStore ks = KeyStore.getInstance("Windows-MY", "SunMSCAPI");
        //Carregando Certificados da KeyStore.
        ks.load(null, null);
        //Obtendo todas as alias de certificados instalados no computador.
        Enumeration<String> aliasesEnum = ks.aliases();

        //Percorrendo todos os certificados da keyStore.
        while (aliasesEnum.hasMoreElements()) {

            //Verificando se o certificado for o certificado de nome repassado.
            if (ks.isKeyEntry(certificadoAlias)) {

                // Carregando pkEntry.
                PrivateKeyEntry pkEntry = (PrivateKeyEntry) ks.getEntry(certificadoAlias, new PasswordProtection(SENHA_KEYSTORE.toCharArray()));
                // Obtendo certificado e cadeia de certificados.
                Certificate[] cadeiaCertificados = ks.getCertificateChain(certificadoAlias);
                // Obtendo chave privada.
                PrivateKey privateKey = pkEntry.getPrivateKey();
                // Cria objeto de assinatura.
                PKCS7Signer signer = PKCS7Factory.getInstance().factoryDefault();
                // Carrega a cadeia de certificados.
                signer.setCertificates(cadeiaCertificados);
                // Carrega a chave privada.
                signer.setPrivateKey(privateKey);
                // Define a politica da assinatura: Legislacao Vigencia: CADES AD-RT-2.2.
                signer.setSignaturePolicy(PolicyFactory.Policies.AD_RB_CADES_2_2);
                // Define o algoritmo para geracao da assinatura.
                signer.setAlgorithm(SignerAlgorithmEnum.SHA256withRSA);
                // Assina o conteudo e verifica
                byte[] signed = signer.doDetachedSign(conteudo);
                //Retorna a assinatura.
                return signed;
            }
        }
        // Retorna null caso não seja assinado.
        return null;
    }

    /**
     * Metodo que valida ou não a assinatura digital
     *
     * @param conteudo
     * @param assinatura
     * @return
     */
    public boolean validar(byte[] conteudo, byte[] assinatura) {
        // Instanciando checador de assinatura.
        CAdESChecker checker = new CAdESChecker();
        // Retorna se a assinatura e valida ou nao.
        return checker.check(conteudo, assinatura);
    }

}
