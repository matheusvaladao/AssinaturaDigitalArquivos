/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avilages.utilitarios;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matheus
 */
public class Conversao {

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    /**
     * Metodo que converte String para Byte
     *
     * @param data
     * @return
     */
    public String converterByteToString(byte[] data) {
        StringBuilder sb = new StringBuilder(data.length);
        for (int i = 0; i < data.length; ++i) {
            sb.append((char) data[i]);
        }
        return sb.toString();
    }

    /**
     * Metodo que converte String para Byte
     *
     * @param str
     * @return
     */
    public byte[] converterStringToByte(String str) {
        byte[] array = new byte[str.length()];
        for (int i = 0; i < str.length(); i++) {
            array[i] = (byte) str.charAt(i);
        }
        return array;
    }

    /**
     * Metodo que converte array de bytes para uma string em hexadecimal
     *
     * @param bytes
     * @return
     */
    public String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    /**
     * Metodo que converte uma string em hexadecimal para um array de bytes
     *
     * @param s
     * @return
     */
    public byte[] hexToBytes(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    /**
     * Converte arquivo file em array de bytes
     *
     * @param file
     * @return
     */
    public byte[] arquivoEmByte(File file) {
        byte[] array = null;
        try {

            int tamanhoArquivo = (int) file.length();
            FileInputStream fis = new FileInputStream(file);

            array = new byte[tamanhoArquivo];
            for (int i = 0; i < tamanhoArquivo; i++) {
                array[i] = (byte) fis.read();
            }

        } catch (Exception ex) {
            System.out.println("Erro ao fazer conversão de arquivo em array de bytes.");
            return null;
        }

        return array;
    }

    /**
     * Método que transforma um array de bytes em um arquivo.
     *
     * @param array
     * @param caminho
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public File byteEmArquivo(byte[] array, String caminho) throws FileNotFoundException, IOException {
        File file = new File(caminho);
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(array);
        fos.close();
        return file;
    }

}
