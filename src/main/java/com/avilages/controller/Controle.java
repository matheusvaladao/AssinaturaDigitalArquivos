/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avilages.controller;

import com.avilages.BouncyCastleAssinatura.AssinaturaCMSSCT;
import com.avilages.BouncyCastleCarimboDoTempo.CarimboDoTempo;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import com.avilages.utilitarios.Conversao;
import java.io.FileInputStream;
import org.bouncycastle.cms.CMSException;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Matheus
 */
public class Controle {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Controle.class);

    /**
     * Metodo que realiza a assinatura de um arquivo com certificado A1.
     *
     * @param arquivo
     * @param certificadoA1
     * @param senha
     * @return 1 : Assinatura realizada com sucesso. 2 : Certificado fora da
     * data de validade. 3 : Senha incorreta. 4: Erro na assinatura.
     */
    public int assinarA1(File arquivo, File certificadoA1, String senha) {

        try {

            Conversao conversao = new Conversao();
            byte[] arrayArquivo = conversao.arquivoEmByte(arquivo);

            //com.avilages.BouncyCastleAssinatura.AssinaturaCMSSCT assinatura = new com.avilages.BouncyCastleAssinatura.AssinaturaCMSSCT();
            //com.avilages.BouncyCastleAssinatura.AssinaturaCMSCCT assinatura = new com.avilages.BouncyCastleAssinatura.AssinaturaCMSCCT();
            //com.avilages.DemoiselleAssinatura.Assinatura assinatura = new com.avilages.DemoiselleAssinatura.Assinatura();
            com.avilages.BouncyCastleAssinatura.AssinaturaCadesAD_RB assinatura = new com.avilages.BouncyCastleAssinatura.AssinaturaCadesAD_RB();
            //com.avilages.BouncyCastleAssinatura.AssinaturaCadesAD_RT assinatura = new com.avilages.BouncyCastleAssinatura.AssinaturaCadesAD_RT();

            byte[] signed = assinatura.assinarA1(arrayArquivo, new FileInputStream(certificadoA1), senha);

            BufferedOutputStream bos = null;
            FileOutputStream fos = new FileOutputStream(new File(arquivo.getAbsolutePath() + ".p7s"));
            bos = new BufferedOutputStream(fos);
            bos.write(signed);
            bos.close();

        } catch (Exception e) {
            System.out.println(e);
            if (e.toString().equals("java.lang.NullPointerException")) {
                return 2;
            }
            if (e.toString().equals("java.io.IOException: keystore password was incorrect")) {
                return 3;
            }
            return 4;
        }
        return 1;
    }

    /**
     * Metodo que realiza a assinatura de um arquivo com certificado A3.
     *
     * @param arquivo
     * @param certificadoAlias
     * @return 1 : Assinatura realizada com sucesso. 2 : Certificado fora da
     * data de validade. 3 : Senha incorreta. 4: Erro na assinatura.
     */
    public int assinarA3(File arquivo, String certificadoAlias) {
        try {
            Conversao conversao = new Conversao();
            byte[] arrayArquivo = conversao.arquivoEmByte(arquivo);

            //com.avilages.BouncyCastleAssinatura.AssinaturaCMSSCT assinatura = new com.avilages.BouncyCastleAssinatura.AssinaturaCMSSCT();
            //com.avilages.BouncyCastleAssinatura.AssinaturaCMSCCT assinatura = new com.avilages.BouncyCastleAssinatura.AssinaturaCMSCCT();
            //com.avilages.DemoiselleAssinatura.Assinatura assinatura = new com.avilages.DemoiselleAssinatura.Assinatura();
            com.avilages.BouncyCastleAssinatura.AssinaturaCadesAD_RB assinatura = new com.avilages.BouncyCastleAssinatura.AssinaturaCadesAD_RB();
            //com.avilages.BouncyCastleAssinatura.AssinaturaCadesAD_RT assinatura = new com.avilages.BouncyCastleAssinatura.AssinaturaCadesAD_RT();
            byte[] signed = assinatura.assinarA3(arrayArquivo, certificadoAlias);

            FileOutputStream fos = new FileOutputStream(new File(arquivo.getPath() + ".p7s"));
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            bos.write(signed);
            bos.close();

        } catch (Exception e) {
            System.out.println(e);
            if (e.toString().equals("java.lang.NullPointerException")) {
                return 2;
            }
            if (e.toString().equals("java.io.IOException: keystore password was incorrect")) {
                return 3;
            }
            return 4;
        }
        return 1;
    }

    /**
     * Método que carrega os arquivos e valida a assinatura.
     *
     * @param arquivoFile
     * @param assinaturaFile
     * @return
     * @throws org.bouncycastle.cms.CMSException
     */
    public boolean validar(File arquivoFile, File assinaturaFile) throws CMSException {

        Conversao conversao = new Conversao();
        byte[] arquivo = conversao.arquivoEmByte(arquivoFile);
        byte[] assinatura = conversao.arquivoEmByte(assinaturaFile);

        logger.info("Checando assinatura.");
        com.avilages.BouncyCastleAssinatura.AssinaturaCadesAD_RT as = new com.avilages.BouncyCastleAssinatura.AssinaturaCadesAD_RT();
        boolean assinaturaDigital = as.validar(arquivo, assinatura);

        // Verifica carimbo do tempo.
        logger.info("Checando carimbo do tempo.");
        CarimboDoTempo carimboDoTempo = new CarimboDoTempo();
        boolean carimboTempo = carimboDoTempo.checarCarimboDoTempo(arquivo, assinatura);

        logger.info("Verificacao completa.");
        if (assinaturaDigital && carimboTempo) {
            logger.info("Assinatura e carimbo do tempo estao ok.");
            return true;
        } else {
            logger.info("Assinatura ou carimbo do tempo esta errado.");
            return false;
        }
    }

    /**
     * Método que retorna o relatório da assinatura criada
     *
     * @param fileOriginal
     * @param fileAssinatura
     * @return
     */
    public String relatorioAssinatura(File fileOriginal, File fileAssinatura) {
        com.avilages.BouncyCastleAssinatura.InformacaoAssinatura iA = new com.avilages.BouncyCastleAssinatura.InformacaoAssinatura();
        Conversao conversao = new Conversao();
        byte[] conteudo = conversao.arquivoEmByte(fileOriginal);
        byte[] assinatura = conversao.arquivoEmByte(fileAssinatura);
        try {
            return (iA.retornaInformacao(conteudo, assinatura));
        } catch (Exception ex) {
            return "Impossível obter informações da assinatura.";
        }
    }

}
